import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FirstViewComponent } from './components/first-view/first-view.component';
import { SecondViewComponent } from './components/second-view/second-view.component';



const routes: Routes = [
  {path: '', redirectTo: '/first-view', pathMatch: 'full'},
  {path: 'first-view', component: FirstViewComponent},

  {path: '', redirectTo: '/second-view', pathMatch: 'full'},
  {path: 'second-view', component: SecondViewComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
