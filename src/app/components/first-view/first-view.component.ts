import { Component, OnInit} from "@angular/core";
import { DataApiService } from "../shared/data-api-firstView/data-api.service";



@Component({
  selector: "app-first-view",
  templateUrl: "./first-view.component.html",
  styleUrls: ["./first-view.component.css"],
  providers: [DataApiService],
})
export class FirstViewComponent implements OnInit {
  // Declaro las variables publicas que van a contener los valores obtenidos.
  public items: any[];
  public acount: any[];
  public numbersNotRepeated: any[];

  constructor(private dataSvc: DataApiService) {}

  ngOnInit() {
    this.getNumbers(); // Le paso la funcion getNumbers a el ngOnInit().
  }

  getNumbers() { // llama la data desde  DataApiService.
    this.dataSvc.getData().subscribe((res: any[]) => {
      if (res) {  // Si existe una respuesta le idico que me la guarde en  el arreglo vacio de this.items declarado al principio.
        this.items = res; // Inicializo la variable con la respuesta.
        console.log(res);
        const ordered = this.numbersOrdered(this.items); // Le paso los valores de this.items a la funcion  numbersOrdered.
        this.numbersNotRepeated = [...new Set(this.items)];  // Le paso un set al array para que elimine los dulicados de numeros y me genere un nuevo array con los numeros sin repetir.
        // console.log(this.numbersNotRepeated);
        this.acount = []; // inicializo el arreglo vacio que va a contener la cantidad de repeticiones de los numeros.

        this.items.forEach(number => { // Aplico un forrEach para recocrrer el arreglo elemento por elemento.
          this.acount[number] = this.acount[number] ? this.acount[number] + 1 : 1; // Le indico que el si el numero existe en el arreglo this.acount le sume uno y no existe lo inicialice en uno.
        });
        console.log(this.acount);
      }
    });
  }

  numbersOrdered(ordered) { // Le paso los valores obtenidos de la data, que estan guardados previamente en this.items.
//  console.log(ordered);

let temp; // Variable temporal para el intercambio de valores de posiciones de los numeros.

for (let i = 0; i <  ordered.length ; i++) { // Recorro los items y los va ordenando en su recorrido.
  for (let j = 0 ; j <  ordered.length - i - 1; j++) {
  if (ordered[j] > ordered[j + 1]) {
    temp = ordered[j];
    ordered[j] = ordered[j + 1];
    ordered[j + 1] = temp;
    // console.log(temp);
  }
 }
}
  }
}
