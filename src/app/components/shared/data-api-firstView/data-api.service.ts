import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataI } from '../data.interface';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable()
export class DataApiService {
  urlApi = 'http://patovega.com/prueba_frontend/array.php';


  constructor(private http: HttpClient) { }

  getData(): Observable<DataI[]> {
    // return this.http.get<DataI[]>(this.urlApi)
    // .pipe(map((data: any) => data.data));

    return this.http.get<DataI[]>(this.urlApi)
    .pipe(map((data: any) => data.data));

    // return this.http.get<any>(this.urlApi);
  }
}
