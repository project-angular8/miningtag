import { TestBed } from '@angular/core/testing';

import { DataApiSecondViewService } from './data-api-second-view.service';

describe('DataApiSecondViewService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DataApiSecondViewService = TestBed.get(DataApiSecondViewService);
    expect(service).toBeTruthy();
  });
});
