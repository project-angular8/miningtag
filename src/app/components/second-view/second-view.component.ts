import { Component, OnInit, ɵConsole } from '@angular/core';
import { DataApiSecondViewService } from '../shared/data-api-secondView/data-api-second-view.service';
import { DataI } from '../shared/data.interface';

@Component({
  selector: 'app-second-view',
  templateUrl: './second-view.component.html',
  styleUrls: ['./second-view.component.css'],
  providers: [DataApiSecondViewService]
})
export class SecondViewComponent implements OnInit {
  // Declaro las variables publicas que van a contener los valores obtenidos.
  public items: any[];
  public letters: string[];
  public sumNumbers: any[];
  


  constructor(private dataSvc: DataApiSecondViewService) { }

  ngOnInit() {
    // Guardo las letras en un string para pasarle los valores a un ngFor en el html y crear la tabla.
    this.letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    this.getCharacters(); // Le paso la funcion getCharacters a el ngOnInit().
  }

  getCharacters() { // llama la data desde  DataApiSecondViewService.
    this.dataSvc.getData().subscribe(
      (res: any[]) => {
      if (res) {
        this.items = []; // inicializo el array vacio items para despues hacerle un push con los valores de la cantidad de letras por parrafo.
        // console.log(res);
        this.sumNumbers = []; // inicializo el array vacio donde hare posteriormente push el arreglo de la suma de numeros por cada parrafo en caso de contener.

        res.forEach(element => {
        //  console.log(element.paragraph);
        const paragraph = element.paragraph.toLowerCase(); // Guardo element por cada parrafo y paso la cadena a minusculas.
        // console.log(paragraph);
         const numbersOfParagraph  = this.getNumbers(paragraph); // Le paso los valores de la const paragraph a la funcion getNumbers.
         const result = [...paragraph].reduce((a, e) => {
            a[e] = a[e] ? a[e] + 1 : 1; return a }, {});  // Le indico que si existe el elemento le sume una al result y si no existe la inicialize en 1.
         console.log(result);
         this.items.push(result);  // le envio los valores de result al arreglo vacio this.items
        });
        // console.log(this.items);
      }
    });
}


getNumbers(numbersOfParagraph) {  // Le paso a la funcion getNumbers los valores de los elementos por parrafo previamente guardados en la const numbersOfParagraph de la funcion getCharacters.
  console.log(numbersOfParagraph);
  const extractNumbers = /(\d+)/gi; // Guardo la expresion regular para determinar si es un numero y pasalo a un entero.
  let saveNumbers = numbersOfParagraph.match(extractNumbers); // Guardo en un let el arreglo de numeros que hagan match con la expresion regular en caso de contener el parrafo.
  console.log(saveNumbers);
  if (saveNumbers) {  // Le incico que de existir numeros me los sume.
    saveNumbers = saveNumbers.reduce((sum, current) => parseInt(sum) + parseInt(current), 0); // Le indico que la variable saveNumbers va a contener ahora la suma de los numeros.
  }
  this.sumNumbers.push(saveNumbers);

  console.log('//////////////////////////////////////////////////////////////////');
  console.log(this.sumNumbers);
  console.log('*******************************************************************');

}
}

